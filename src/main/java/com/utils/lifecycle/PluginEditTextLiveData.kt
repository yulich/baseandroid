package com.utils.lifecycle

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.lifecycle.LiveData
import java.lang.ref.WeakReference

data class TextWatcherUpdated(val text: String, val selection: Int)

interface EditTextLiveDataListener {
    fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int)
    fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int): TextWatcherUpdated?
}

abstract class PluginEditTextLiveData(value: String = "") : LiveData<String>(value) {
    private var skipSet = false
    private var editTextRef: WeakReference<EditText>? = null
    private var liveDataListener: EditTextLiveDataListener? = null

    private val textWatcher: TextWatcher by lazy {
        object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                liveDataListener?.beforeTextChanged(s, start, count, after)
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val listener = liveDataListener
                if (listener != null) {
                    editTextRef?.get()?.also { editText ->
                        val updated = listener.onTextChanged(s, start, before, count)
                        if (updated == null) {
                            if (!skipSet) {
                                this@PluginEditTextLiveData.setLiveDataValue(s.toString())
                            }
                        } else {
                            editText.removeTextChangedListener(this)
                            editText.setText(updated.text)
                            editText.setSelection(updated.selection)
                            editText.addTextChangedListener(this)

                            this@PluginEditTextLiveData.setLiveDataValue(updated.text)
                        }
                    }
                } else {
                    if (!skipSet) {
                        this@PluginEditTextLiveData.setLiveDataValue(s.toString())
                    }
                }

                if (skipSet) {
                    skipSet = false
                }
            }

            override fun afterTextChanged(editable: Editable?) {}
        }
    }

    private fun setLiveDataValue(value: String) {
        super.setValue(value)
    }

    fun setTextListener(textLiveDataListener: EditTextLiveDataListener) {
        liveDataListener = textLiveDataListener
    }

    var bindingEditText: EditText?
        get() {
            return editTextRef?.get()
        }
        set(view) {
            editTextRef?.get()?.also { lastEdit ->
                lastEdit.removeTextChangedListener(textWatcher)
                editTextRef = null
            }

            if (view != null) {
                editTextRef = WeakReference(view)

                view.addTextChangedListener(textWatcher)
                val liveDataValue = value ?: ""
                if (liveDataValue != view.text.toString()) {
                    skipSet = true
                    view.setText(liveDataValue)
                    view.setSelection(liveDataValue.length)
                }
            }
        }

    override fun setValue(value: String?) {
        when (val editText = editTextRef?.get()) {
            null -> super.setValue(value)
            else -> editText.setText(value)
        }
    }

    override fun postValue(value: String?) {
        when (val editText = editTextRef?.get()) {
            null -> super.postValue(value)
            else -> editText.setText(value)
        }
    }
}