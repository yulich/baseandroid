package com.utils.lifecycle

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.lifecycle.LiveData
import java.lang.ref.WeakReference

abstract class EditTextLiveData(value: String = "") : LiveData<String>(value) {
    private var skipSet = false
    private var editTextRef: WeakReference<EditText>? = null
    private val textWatcher: TextWatcher by lazy {
        object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (skipSet) {
                    skipSet = false
                } else {
                    this@EditTextLiveData.setLiveDataValue(s.toString())
                }
            }

            override fun afterTextChanged(editable: Editable?) {}
        }
    }

    private fun setLiveDataValue(value: String) {
        super.setValue(value)
    }

    var bindingEditText: EditText?
        get() {
            return editTextRef?.get()
        }
        set(view) {
            editTextRef?.get()?.also { lastEdit ->
                lastEdit.removeTextChangedListener(textWatcher)
                editTextRef = null
            }

            if (view != null) {
                editTextRef = WeakReference(view)

                view.addTextChangedListener(textWatcher)
                val liveDataValue = value ?: ""
                if (liveDataValue != view.text.toString()) {
                    skipSet = true
                    view.setText(liveDataValue)
                    view.setSelection(liveDataValue.length)
                }
            }
        }

    override fun setValue(value: String?) {
        when (val editText = editTextRef?.get()) {
            null -> super.setValue(value)
            else -> editText.setText(value)
        }
    }

    override fun postValue(value: String?) {
        when (val editText = editTextRef?.get()) {
            null -> super.postValue(value)
            else -> editText.setText(value)
        }
    }
}