package com.utils.extension

import android.annotation.SuppressLint
import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.core.view.postDelayed

fun Activity.showKeyboard(editText: EditText) {
    editText.postDelayed(50) {
        editText.requestFocus()
        val manager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        manager.showSoftInput(editText, 0)
    }
}

fun Activity.hideKeyboard() {
    val windowToken = findViewById<View>(android.R.id.content).rootView.windowToken
    val manager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    manager.hideSoftInputFromWindow(windowToken, 0)
}

@SuppressLint("ClickableViewAccessibility")
fun Activity.registerHideKeyboardOnTouchContentView() {
    val contentContainer = findViewById<ViewGroup>(android.R.id.content)
    contentContainer?.setOnTouchListener { view, _ ->
        hideKeyboard()
        false
    }
}