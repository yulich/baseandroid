package com.utils.extension

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.consumeAsFlow

fun <T> Channel<T>.throttleFirstAsFlow(duration: Long): Flow<T> = consumeAsFlow().throttleFirst(duration)