package com.utils.extension

import kotlinx.coroutines.flow.*

fun <T> Flow<T>.throttleFirst(duration: Long): Flow<T> = flow {
    var lastEmitTime = 0L
    collect { value ->
        val currentTime = System.currentTimeMillis()
        if (currentTime - lastEmitTime > duration) {
            lastEmitTime = currentTime
            emit(value)
        }
    }
}
