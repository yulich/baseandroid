package com.utils.extension

import android.view.View

fun View.setOnClickThrottleFirst(duration: Long, block: () -> Unit) {
    var lastInvokeTime = 0L
    setOnClickListener {
        val currentTime = System.currentTimeMillis()
        if (currentTime - lastInvokeTime > duration) {
            lastInvokeTime = currentTime
            block.invoke()
        }
    }
}
