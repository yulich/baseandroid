package com.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.utils.lifecycle.autoCleared

abstract class BaseFragment<out B : ViewBinding> : Fragment() {

    private var _viewBinding by autoCleared<B>()
    val binding: B get() = _viewBinding

    abstract fun getViewBinding(inflater: LayoutInflater, container: ViewGroup?): B

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return getViewBinding(inflater, container).run {
            _viewBinding = this
            root
        }
    }

    open fun navigateTo(item: NavigateItem) {
        findNavController().also { navController ->
            when (item) {
                NavigateItem.Up -> navController.navigateUp() //.popBackStack()
                is NavigateItem.PopBackStack -> navController.popBackStack(
                    item.fragmentId,
                    item.inclusive
                )
                is NavigateItem.Destination -> {
                    if (item.bundle == null) {
                        navController.navigate(item.action)
                    } else {
                        navController.navigate(item.action, item.bundle)
                    }
                }
            }
        }
    }
}